# AngularJS Directives

## Introduction

In AngularJS directives is a technique to add dynamic functionality to your HTML code. You can add this dynamic functionality
to your HTML as a tag, attribute or a CSS class to your HTML. AngularJS will then attach the functionality to where you 
specified it.

A directive in AngularJS can be specified in the following way.

```
angular
    .module('app')
    .factory('nameOfDirective', function() {
        return {
            restrict: 'C',
            template: '<div class="directive"></div>'
        }
    });
```

## How to apply a directive to an element

You can apply a directive in 3 (or 4) distinct ways on your HTML (by specifying the restrict property). You can combine the different restricts so that you can use the directive as a tag directive, attribute directive or a class directive (e.g. 'AEC').

### Tag directive
```
// Javascript
angular
    .module('app')
    .directive('nameOfDirective', function() {
        return {
            restrict: 'E',
            template: '<div class="directive">content</div>'
        }
    });

// HTML
<name-of-directive />
```

### Attribute directive
```
// Javascript
angular
    .module('app')
    .directive('nameOfDirective', function() {
        return {
            restrict: 'A',
            template: '<div class="directive">content</div>'
        }
    });

// HTML
<div name-of-directive></div>
```

### Class directive
```
// Javascript
angular
    .module('app')
    .directive('nameOfDirective', function() {
        return {
            restrict: 'C',
            template: '<div class="directive">content</div>'
        }
    });

// HTML
<div class="name-of-directive"></div>
```

## Properties of the directive

When creating a directive, you specify several properties like restrict, template etc.

Some of the more important ones are, **restrict**, **scope**, **templateUrl**, **controllerAs** and **controller**.

### Restrict
The restrict property defines how you can apply the directive in your HTML templates, either via a tag (E), a class (C), or an attribute (A). You can combine several of these to allow to specify your directive in different ways into your HTML. So a ```restrict: 'AEC'``` would allow to set your directive in three diferent ways, see above for examples.

### Scope
The scope property is very important in directives, as it specifies which scope you are using inside your directive. For example, when no scope is specified (or false). The scope of the controller is used and when you interpolate a variable in your directive it will use the variable that was bound to the controller. If you do specify a scope it will limit it to either the child scopes, or a isolated scope.

Lets say you have a controller **C**, and in the template there is a directive **D** without a scope specified. Then, if **D** uses the interpolation ```{{variable}}``` it will take the value that was bound in controller **C** to ```$scope```.

Lets say you now have a directive **D** with a scope specified as ```scope: { variable: '=' }```, then you no longer have access to the variables defined in ```$scope``` of the controller and only those that have been set in the scope of your directive **D**, so ```variable```.

There is one more type of scoping in AngularJS, and that is ``scope: true`` it is called the child scope, and it it shared among all directives on an element. This allows to still access the controller scope, unless one of the directive overrides it.

### Template Url
A link to a HTML file that contains the template HTML for this directive.

### Controller
You can connect a controller to a directive, what this means is that a controller function constructor is initiated and maintained by AngularJS at any given time in the directives lifecycle.

### Controller As
The controller is identified in the template as this name. See it as ```$scope.controllerAs``` in the directives scope. So if you have a variable just in the scope of your directive, you would use ``{{variable}}`` and if it is in the controller you would do ``{{controllerAs.variable}}``. 

## Other
There are more options you can find them here: https://code.angularjs.org/1.5.5/docs/api/ng/service/$compile#directive-definition-object

## AngularJS Components

Components are a new concept introduced in the more recent AngularJS versions. See https://code.angularjs.org/1.5.5/docs/guide/component for the docs. See specific this part of the docs for the similarities and differences between a directive and a component https://code.angularjs.org/1.5.5/docs/guide/component#comparison-between-directive-definition-and-component-definition.

Iin short, a component is a simplified directive with more sane defaults. They work very similar to the Web Components in HTML5, and Components in Angular 2. 

An example of a component

```
angular
    .module('app')
    .component('nameOfComponent', {
        templateUrl: 'template.html',
        controller: function ComponentController() {
            var vm = this;

            // Do something with binding
            vm.variable += 1;
        },
        controllerAs: 'vm',
        bindings: {
            variable: '<'
        }
    });

```

A component is not a complete replacement for directives, you are more free and have more options with directives. However, a component reduces common errors and issues by having better default limitations on scope, and how to bind, requirement for a controller, etc.

A component is always used as a HTML tag, so in the example above it would be ``<name-of-component></name-of-component>``. 

## Bindings

There are several type of bindings that can be used in a directive and components. These are

- **@** A string representation of the value.
- **<** A one-way bind of a variable.
- **=** A two-way bind of a variable.
- **&** A function pointer bind.

```
// Controller
$scope.simpleString = "Hello Worlds";
$scope.oneWay = "OneWay";
$scope.twoWay = "TwoWay";
$scope.func = function() {
    return "Func";
}

// Component
angular
    .module('app')
    .component('nameOfComponent', {
        template: '<div>{{vm.stringRep}}, {{vm.oneWay}}, {{vm.twoWay}}, {{vm.func()}}</div>',
        controller: function ComponentController() {
            var vm = this;
        },
        controllerAs: 'vm',
        bindings: {
            stringRep: '@',
            oneWay: '<',
            twoWay: '=',
            func: '&'
        }
    });

// HTML
<name-of-component string-rep="simpleString" one-way="oneWay" two-way="twoWay" func="func()"></name-of-component>

// Output
simpleString, OneWay, TwoWay, Func
```




